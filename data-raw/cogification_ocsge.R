
# cogification_ocsge

# librairies ------
library(RPostgreSQL)
library(DBI)
library(dplyr)
library(COGiter)

rm(list = ls())

drv <- dbDriver("PostgreSQL")
con_datamart <- dbConnect(drv, 
                          dbname="datamart", 
                          host=Sys.getenv("server"), 
                          port=Sys.getenv("port"),
                          user=Sys.getenv("userid"),
                          password=Sys.getenv("pwd_does"))
postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'") 

source_ocsge<-dbReadTable(con_datamart,c("portrait_territoires","source_ocsge"))

cogifiee_ocsge<-cogifier(source_ocsge %>% rename(DEPCOM=depcom))

dbWriteTable(con_datamart,
             c("portrait_territoires","cogifiee_ocsge"),
             cogifiee_ocsge,
             row.names=FALSE, 
             overwrite=TRUE)

dbDisconnect(con_datamart)

rm(list=ls())
