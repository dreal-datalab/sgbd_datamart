
# chargement_rpls

# librairies -------
library(propre.rpls)
library(tidyr)
library(tidyverse)
library(COGiter)
library(lubridate)
library(readr)
library(datalibaba)
library(DBI)
library(RPostgreSQL)
library(googlesheets4)

rm(list = ls())

source(file = "R/dataprep_all.R")

rpls_prep <- dataprep_all()

#liste des communes des EPCI de la région Pays de la Loire
communes_region52 <- communes %>%
  filter(grepl("52", REGIONS_DE_L_EPCI)) %>%
  mutate(DEPCOM = as.character(DEPCOM))  %>%
  pull(DEPCOM)

rpls <- rpls_prep %>%
  filter(TypeZone %in% c("Départements", "Régions", "Epci", "France") | CodeZone %in% communes_region52 ) %>%
  mutate(date = make_date(as.character(millesime), 01, 01),
         across(where(is.character), as.factor)) %>%
  select(-millesime) %>%
  mutate_if(is.factor,as.character)


# dictionnaire de données  rpls et rp du package propre.rpls ------------

# https://gitlab.com/rdes_dreal/propre.rpls/-/blob/master/inst/rstudio/templates/project/ressources/extdata/
# dico_var.csv, dico_var_compl.csv et dico_var_rp.csv téléchargés dans exdata du projet sgbd_datamart

url_dicos <- "https://gitlab.com/rdes_dreal/propre.rpls/-/raw/master/inst/rstudio/templates/project/ressources/extdata/"

dico_var <- read_csv2(paste0(url_dicos, "dico_var.csv")) %>%
  dplyr::bind_rows(read_csv2(paste0(url_dicos, "dico_var_compl.csv"))) %>% 
  dplyr::bind_rows(read_csv2(paste0(url_dicos, "dico_var_rp.csv"))) %>%
  select(-c(Chapitre, usage))

dico_var <- semi_join(dico_var, tibble(nom_court = names(rpls))) %>% 
  # ajout des libellés pour TypeZone CodeZone Zone et date
  bind_rows(
    tribble(
      ~nom_court, ~libelles,
      "TypeZone", "Type de zone" ,
      "Zone" , "Nom de la zone",
      "CodeZone" , "Code INSEE de la zone",
      "date", "Millesime"
    )
  ) 

# # versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = rpls,
            table = "cogifiee_rpls",
            schema = "portrait_territoires",
            db = "datamart",
            user = "does",
            pk = c("TypeZone", "CodeZone", "date"),
            post_row_name = FALSE, 
            droits_schema = TRUE,
            overwrite = TRUE)

post_dico_attr(dico = dico_var,
               table = "cogifiee_rpls",
               schema = "portrait_territoires",
               db = "datamart",
               user = "does")

# rm(list=ls())


# METADONNEES------------------------------------

## récupération du nom du présent script source
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'


## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>%
  # ajout de complement sur la généalogie
  paste0(".\n", "Chargement des donn\u00e9es sur le package propre.rpls")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "cogifiee_rpls", 
                user = "does")

 