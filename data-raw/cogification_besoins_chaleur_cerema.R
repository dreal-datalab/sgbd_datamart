
# cogification_besoins_chaleur_cerema

# librairies ------
library(DBI)
library(RPostgreSQL)
library(dplyr)
library(COGiter)

rm(list = ls())


drv <- dbDriver("PostgreSQL")
con_datamart <- dbConnect(drv, 
                          dbname="datamart", 
                          host=Sys.getenv("server"), 
                          port=Sys.getenv("port"),
                          user=Sys.getenv("userid"),
                          password=Sys.getenv("pwd_does"))
postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'") 

source_besoins_chaleur_cerema<-dbReadTable(con_datamart,c("portrait_territoires","source_besoins_chaleur_cerema"))

cogifiee_besoins_chaleur_cerema<-cogifier(source_besoins_chaleur_cerema %>% rename(DEPCOM=depcom))

dbWriteTable(con_datamart,
             c("portrait_territoires","cogifiee_besoins_chaleur_cerema"),
             cogifiee_besoins_chaleur_cerema,
             row.names=FALSE, 
             overwrite=TRUE)

dbDisconnect(con_datamart)

rm(list=ls())
