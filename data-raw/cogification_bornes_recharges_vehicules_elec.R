
# cogification_bornes_recharges_vehicules_elec

# librairies ------
library(DBI)
library(RPostgreSQL)
library(dplyr)
library(COGiter)

rm(list = ls())


drv <- dbDriver("PostgreSQL")
con_datamart <- dbConnect(drv, 
                          dbname="datamart", 
                          host=Sys.getenv("server"), 
                          port=Sys.getenv("port"),
                          user=Sys.getenv("userid"),
                          password=Sys.getenv("pwd_does"))
postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'") 

source_bornes_recharges_vehicules_elec<-dbReadTable(con_datamart,c("portrait_territoires","source_bornes_recharges_vehicules_elec"))

cogifiee_bornes_recharges_vehicules_elec<-cogifier(source_bornes_recharges_vehicules_elec %>% rename(DEPCOM=depcom))

dbWriteTable(con_datamart,
             c("portrait_territoires","cogifiee_bornes_recharges_vehicules_elec"),
             cogifiee_bornes_recharges_vehicules_elec,
             row.names=FALSE, 
             overwrite=TRUE)

dbDisconnect(con_datamart)

rm(list=ls())