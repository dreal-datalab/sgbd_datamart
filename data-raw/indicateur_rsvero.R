
# indicateur_rsvero

# librairies ------
library(dplyr)
library(tidyr)
library(lubridate)
library(readr)
library(DBI)
library(RPostgreSQL)

rm(list = ls())


# chargement des données ----------
drv <- dbDriver("PostgreSQL")
con_datamart <- dbConnect(drv, 
                          dbname="datamart", 
                          host=Sys.getenv("server"), 
                          port=Sys.getenv("port"),
                          user=Sys.getenv("userid"),
                          password=Sys.getenv("pwd_does"))
postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'") 

cogifiee_rsvero<-dbReadTable(con_datamart,c("portrait_territoires","cogifiee_rsvero"))

data_cogifiee<-pivot_longer(cogifiee_rsvero,
                            cols = pl_autres_energies:vul_hybride_rechargeable,
                            names_to = "variable",
                            values_to = "valeur")


# calcul ------------
indicateur_rsvero<-data_cogifiee %>% 
  pivot_wider(names_from = "variable",values_from='valeur') %>% 
  rowwise() %>% 
  mutate(part_vehicule_pl_electrique_hydrogene_hybride_rechargeable_pourcent = 
           100*pl_electrique_et_hydrogene / sum(c_across(starts_with("pl_"))),
         part_vehicule_tcp_electrique_hydrogene_hybride_rechargeable_pourcent = 
           100*tcp_electrique_et_hydrogene / sum(c_across(starts_with("tcp_"))),
         part_vehicule_vp_electrique_hydrogene_hybride_rechargeable_pourcent = 
           100*(vp_electrique_et_hydrogene + vp_hybride_rechargeable) / sum(c_across(starts_with("vp_"))),
         part_vehicule_vul_electrique_hydrogene_hybride_rechargeable_pourcent = 
           100*(vul_electrique_et_hydrogene + vul_hybride_rechargeable) / sum(c_across(starts_with("vul_"))),
         part_vehicule_electrique_hydrogene_hybride_rechargeable_pourcent = 
           100*sum(c_across(ends_with(c("_electrique_et_hydrogene","_hybride_rechargeable")))) / sum(c_across(starts_with(c("pl_","tcp_","vp_","vul_")))),
         ) %>% 
  ungroup() %>% 
  select(TypeZone,Zone,CodeZone,date,starts_with('part_')) %>% 
  pivot_longer(cols =starts_with('part'),names_to = "variable",values_to = "valeur") %>% 
  mutate(variable = as.factor(variable)) 


# # libellé des variables (partie neutralisée) --------
# lib_var <- data.frame(
#   source = "indicateur_rsvero",
#   variable=levels(indicateur_rsvero$variable),
#   libelle_variable = c("Part des véhicules électrique, hydrogène et hybride rechargeable",
#                        "Part des véhicules PL électrique, hydrogène et hybride rechargeable",
#                        "Part des véhicules TCP électrique, hydrogène et hybride rechargeable",
#                        "Part des véhicules VP électrique, hydrogène et hybride rechargeable",
#                        "Part des véhicules VUL électrique, hydrogène et hybride rechargeable"),
#   unite = "%",
#   secret = "non")
# write_excel_csv2(lib_var,'data/metadata/indicateurs_rsvero.csv')


# versement dans le sgbd/datamart.portrait_territoires -------------
indicateur_rsvero<-indicateur_rsvero %>% 
  pivot_wider(names_from = variable,values_from = valeur)

drv <- dbDriver("PostgreSQL")
con_datamart <- dbConnect(drv, 
                          dbname="datamart", 
                          host="10.44.128.174", 
                          port=5432,
                          user="does",
                          password=Sys.getenv("pwd_does"))
postgresqlpqExec(con_datamart, "SET client_encoding = 'windows-1252'")

dbWriteTable(con_datamart, c("portrait_territoires","indicateur_rsvero"),
             indicateur_rsvero, row.names=FALSE, overwrite=TRUE)

dbDisconnect(con_datamart)

rm(list=ls())
