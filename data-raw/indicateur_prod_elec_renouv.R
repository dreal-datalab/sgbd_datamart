# indicateur_prod_elec_renouvelable

# 2 indicateurs : total elec renouvelable produite et calcul % enr dans conso

# librairies ------
library(dplyr)
library(tidyr)
library(dplyr)
library(COGiter)
library(datalibaba)

rm(list = ls())


# chargement data ----------
cogifiee_conso_elec_179 <- importer_data(db = "datamart", schema = "portrait_territoires", table = "cogifiee_conso_elec_179", user = "does")

conso_elec_179 <- pivot_longer(cogifiee_conso_elec_179, 
                               cols = !c(contains("Zone"), contains("zone"), date),
                               names_to = "variable", values_to = "valeur")

cogifiee_prod_elec_renouv <- importer_data(db = "datamart", schema = "portrait_territoires", table = "cogifiee_prod_elec_renouv", user = "does")

prod_elec_renouv <- pivot_longer(cogifiee_prod_elec_renouv,
                                 cols =  -c(contains("Zone"), contains("zone"), date),
                                 names_to = "variable", values_to = "valeur")


# calcul --------
conso_elec_179 <- filter(conso_elec_179, variable == "conso_elec_gwh") %>%
  pivot_wider(names_from = variable, values_from = valeur)

indicateur_prod_elec_renouv <- prod_elec_renouv %>% 
  filter(grepl("kWh", variable)) %>% 
  group_by(date, CodeZone, TypeZone) %>%
  summarise(GWh_enr_elec=(sum(valeur, na.rm = TRUE)/1000000) %>% round(3)) %>%
  ungroup() %>% 
  left_join(conso_elec_179) %>%
  mutate(part_elec_renouv = GWh_enr_elec/conso_elec_gwh*100) %>%
  select(-conso_elec_gwh) %>% 
  mutate_if(is.character, as.factor)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = indicateur_prod_elec_renouv, db = 'datamart', schema = "portrait_territoires", table = "indicateur_prod_elec_renouv",
            pk = c("TypeZone", "CodeZone", "date"), post_row_name = FALSE, overwrite = TRUE, droits_schema = TRUE, user = "does")


rm(list=ls())
