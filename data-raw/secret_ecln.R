
# secret_ecln

# librairies -------------
library(dplyr)
library(tidyr)
library(lubridate)
library(COGiter)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


# chargement data -------------
cogifiee_ecln <- importer_data(db = "datamart",
                               schema = "portrait_territoires",
                               table = "cogifiee_ecln")

data_cogifiee<-pivot_longer(cogifiee_ecln,
                cols = nb_logt_en_cours.collectif : surface_totale_des_ventes.total,
                names_to = "variable",
                values_to = "valeur") %>%
  mutate_if(is.character,as.factor)

tranches_ecln <- read.csv2('metadata/secret_ecln.csv', as.is = TRUE, encoding = "UTF-8" )

data_ecln <- left_join(data_cogifiee, liste_zone, by= c("CodeZone","TypeZone","Zone")) %>%
  left_join(epci %>% select(EPCI,NOM_EPCI), by = "EPCI")


# distinction des données à secrétiser ---------

# listes des variables logt et promoteurs
variables_logt <- tranches_ecln  %>%
  pull(variable)
variables_promoteurs <- tranches_ecln  %>%
  pull(variable_associee)  %>%
  unique()

# table des nb promoteurs
data_promoteurs <- data_ecln  %>%
  filter (variable %in% variables_promoteurs) %>%
  rename(variable_associee=variable, nb_promoteurs=valeur)

#table des données
data_ecln_donnees <- data_ecln  %>%
  filter (variable %in% variables_logt)

data_ecln_donnees <- left_join(data_ecln_donnees, tranches_ecln, by= "variable")
data_ecln_donnees <- left_join(data_ecln_donnees, data_promoteurs %>%
                                 select(TypeZone,CodeZone,date,variable_associee,nb_promoteurs),
                               by= c("TypeZone","CodeZone","date", "variable_associee") )

#liste des variables publiques ou non
variables_publiques <- tranches_ecln  %>%
  filter(secret=="0")  %>%
  pull(variable)
variables_non_publiques <- tranches_ecln  %>%
  filter(secret !="0")  %>%
  pull(variable)

#table des données publiques
data_ecln_A <- data_ecln_donnees  %>%
  filter (variable %in% variables_publiques|
            TypeZone %in% c("Régions","Départements","France"))  %>%
  select(-secret,-variable_associee,-categorie)  %>%
  mutate(valeur=as.character(valeur))

#table des données à secrétiser
data_ecln_B <- data_ecln_donnees  %>%
  filter (variable %in% variables_non_publiques &
            TypeZone %in% c("Communes","Epci"))


# secrétisation des données -----------
secret_communes<-data_ecln_B %>% 
  filter(TypeZone=="Communes")

#secret induit, pour une meme variable et un meme epci, pour ne pas retrouver la valeur d'une commune
#en faisant la somme des communes

secret_communes<-secret_communes %>% 
  group_by(variable,EPCI,date) %>%
  mutate(A_nb_inf3=length(which(nb_promoteurs<3))) %>% #compte combien inférieur à 3
  mutate(A_rang=rank(nb_promoteurs, ties.method = "first")) #classe pour repérer les 2 plus petites valeurs
secret_communes$A_sec<-case_when(
  secret_communes$nb_promoteurs<3 ~ 1,      #secret pour toutes les nb_promoteurs inférieurs à 3
  secret_communes$A_nb_inf3== 0 ~ 0,   #enlève le secret si aucune des communes rang 1 et 2 inférieur à 11
  secret_communes$A_rang<3 ~ 1,         #secret sur les 2 communes avec valeurs les plus basses
  TRUE ~ 0)

# secret induit, pour une meme categorie et une meme commune, pour ne pas retrouver la valeur d'une variable
# en faisant la somme des variables de la commune

secret_communes<-secret_communes %>%
  group_by(categorie,Zone,date) %>%
  mutate(B_nb_inf3=length(which(nb_promoteurs<3))) %>% #compte combien inférieur à 3
  mutate(B_rang=rank(nb_promoteurs, ties.method = "first")) #classe pour repérer les 2 plus petites valeurs
secret_communes$B_sec<-case_when(
  secret_communes$B_nb_inf3== 0 ~ 0,   #enlève le secret si aucune des communes rang 1 et 2 inférieur à 11
  secret_communes$B_rang<3 ~ 1,         #secret sur les 2 communes avec valeurs les plus basses
  TRUE ~ 0)  

# regroupement des secrets, masquage des valeurs
secret_communes<-secret_communes %>%
  mutate(valeur=as.character(valeur))
secret_communes$valeur<-case_when(
  is.na(secret_communes$nb_promoteurs)~ "nc",
  secret_communes$B_sec== 1 ~ "nc",         #remplace valeur par "nc" si secret stat
  secret_communes$A_sec== 1 ~ "nc",         #remplace valeur par "nc" si secret stat
  TRUE ~ secret_communes$valeur)
secret_communes<-secret_communes %>%
  ungroup() %>%
  select(TypeZone,Zone,CodeZone,date,variable,valeur,EPCI,NATURE_EPCI,DEP,REG,NOM_EPCI,nb_promoteurs)


# secretisation des EPCI -----

secret_epci<- data_ecln_B %>%
  filter(TypeZone =="Epci")

# secret induit, pour une meme categorie et un meme epci, pour ne pas retrouver la valeur d'une variable
# en faisant la somme des variables de l'Epci

secret_epci<-secret_epci %>%
  group_by(categorie,CodeZone,date) %>%
  mutate(B_nb_inf3=length(which(nb_promoteurs<3))) %>% #compte combien inférieur à 3
  mutate(B_rang=rank(nb_promoteurs, ties.method = "first")) #classe pour repérer les 2 plus petites valeurs
secret_epci$B_sec<-case_when(
  secret_epci$valeur<3 ~ 1,      #secret pour toutes les valeurs inférieures à 11  
  secret_epci$B_nb_inf3== 0 ~ 0,   #enlève le secret si aucune des communes rang 1 et 2 inférieur à 11
  secret_epci$B_rang<3 ~ 1,         #secret sur les 2 communes avec valeurs les plus basses
  TRUE ~ 0)  

# regroupement des secrets, masquage des valeurs
secret_epci<-secret_epci %>%
  mutate(valeur=as.character(valeur))
secret_epci$valeur<-case_when(
  is.na(secret_epci$nb_promoteurs)~ "nc",
  secret_epci$B_sec== 1 ~ "nc",         #remplace valeur par "nc" si secret stat
  TRUE ~ secret_epci$valeur)
secret_epci<-secret_epci %>%
  ungroup() %>%
  select(TypeZone,Zone,CodeZone,date,variable,valeur,EPCI,NATURE_EPCI,DEP,REG,NOM_EPCI,nb_promoteurs)


# regroupement des zonages ------------
data_ecln_secretise<-bind_rows(secret_communes,secret_epci,data_ecln_A)

secretise_ecln <- data_ecln_secretise  %>%
  select(-EPCI,-NATURE_EPCI,-DEP,-REG,-NOM_EPCI,-nb_promoteurs)

# remplace "nc" par NA    
secretise_ecln$valeur<-na_if(secretise_ecln$valeur,"nc")

secretise_ecln <- secretise_ecln %>%
  mutate(valeur=as.numeric(valeur)) %>%
  mutate_if(is.factor,as.character) %>% 
  pivot_wider(names_from = variable,values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = secretise_ecln,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "secretise_ecln",
            pk = c("TypeZone", "Zone", "CodeZone", "date"),
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            user = "does")

# commentaires de la table et des variables -------------

# récupération des commentaires de la table source
dico_var <- get_table_comments(
  db = "datamart",
  schema = "portrait_territoires",
  table = "cogifiee_ecln",
  user = "does")

# commentaire de la table
comm_table <- filter(dico_var, is.na(nom_col)) %>% 
  pull(commentaire) %>% 
  gsub("\nCommentaire.*$", "", .)

commenter_table(
  comment = comm_table,
  db = "datamart",
  schema = "portrait_territoires",
  table = "secretise_ecln",
  user = "does"
)

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()

## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == "secret_ecln") %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "TypeZone", "Type de territoire",
      "Zone", " Nom du territoire",
      "CodeZone", "Code INSEE du territoire",
      "date", "Millesime"
    )
  )

post_dico_attr(
  dico = metadata_indicateur,
  db = "datamart",
  schema = "portrait_territoires",
  table = "secretise_ecln",
  user = "does"
)
