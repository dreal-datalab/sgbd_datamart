
# chargement_bornes_recharges_vehicules_elec

# Indicateurs CRTE : Nombre de bornes de recharge ouvertes au public

# librairies ----------
library(tidyverse)
library(COGiter)
library(lubridate)
# remotes::install_gitlab("dreal-datalab/rapport.irve")
library(rapport.irve)
library(sf)
library(datalibaba)

rm(list = ls())

# Voir les couches chargées dans le SGBD dans le schéma dédié aux bornes IRVE
couches_disp <- list_couches()
dates_maj <- list_couches() %>% substr(18, 28)

# si la dernière couche est ancienne lancer mettre à jour la collecte
# mettre_a_jour_la_collecte()

# une fonction pour lire et combiner les couches geo des bornes de recharge
prep_irve <- function(i) {
  lect_data(couche = list_couches()[i]) %>% 
    mutate(date = dates_maj[i])
}

# lect_data charge la dernière couche, quand on aura 12 mois d'écarts entre deux couches, il faudra mettre à jour le script pour pouvoir calculer des évolutions
irve <- map_dfr(.x = c(1:length(couches_disp)), .f = ~ prep_irve(.x))

# communes x dates : un dataframe croisant dates de mise à jour et liste des communes pour avoir les communes sans bornes de recharge pour tous les millésiomes
com_dates = communes %>% 
  select(depcom = DEPCOM) %>% 
  mutate(date = list(dates_maj)) %>% 
  unnest(date)

# calculs des indicateurs : nb de stations et nb de prises par communes ------------
indic_irve <- irve %>% 
  st_drop_geometry() %>% 
  select(date, depcom = DEPCOM, id_geo, nbre_pdc) %>% 
  group_by(date, depcom) %>% 
  summarise(nb_stations = n_distinct(id_geo), nb_pdc = sum(nbre_pdc), .groups = "drop") %>% 
  full_join(com_dates, by=c("depcom", "date")) %>% 
  replace_na(list("nb_stations" = 0, "nb_pdc" = 0)) %>% 
  mutate(across(where(is.character), as.factor)) %>% 
  mutate(date= as.Date(date))


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = indic_irve, table = "source_bornes_recharges_vehicules_elec", schema = "portrait_territoires",
            pk = c("date", "depcom"), post_row_name = FALSE, overwrite = TRUE, db = "datamart")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(indic_irve), c("depcom", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()

## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "depcom", "Code INSEE de la commune",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "source_bornes_recharges_vehicules_elec", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>% 
  # ajout de complement sur la généalogie
  paste0(".\n", "")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "source_bornes_recharges_vehicules_elec", 
                user = "does")
# ----------

rm(list=ls())
