
# chargement_conso_elec

# consommation electrique art 179 ltecv fournies par le SDES

# on récupère les données préalablement nettoyées dans le cadre du POC ENR TEO dans le SGBD
# script de nettoyage : https://gitlab.com/dreal-datalab/enr_reseaux_teo/-/tree/dev/collecte/conso_elec
# initialement fournies par https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie 


# librairies ---------
library(dplyr)
library(tidyr)
library(forcats)
library(stringr)
library(readr)
library(purrr)
library(COGiter)
library(lubridate)
library(datalibaba)
library(googlesheets4)

rm(list=ls())


# paramètres -----------
mill <- 2020
sgbd <- FALSE

if(sgbd) {
  adr_enr_teo <- ""
} else {
  adr_enr_teo <- "C:/Users/juliette.engelaere/Documents/Travail/R_local/enr_reseaux_teo/" 
  # ne fonctionne que sur le poste de JEL si sgbd <- FALSE
}


# lecture des données versées dans le sgbd dans le cadre du POC ENR TEO --------
if (sgbd) {

  conso_elec_179_0 <- importer_data(table = paste0("conso_elec_2008_", mill, "_frce_entiere"), 
                                    schema = "electricite", db = "production")
  
} else {
  load(paste0(adr_enr_teo, "collecte/sgbd/conso_elec_2008-", mill, "_Frce_entiere.RData"))
  conso_elec_179_0 <- compil_tot %>% 
    rename_all(tolower)
  load(paste0(adr_enr_teo, "collecte/sgbd/spec_conso.RData"))
}


# début dataprep -------------
conso_elec_179_1 <- conso_elec_179_0 %>%
  filter(!grepl("ZZZ", id_ter), echelle != "iris") %>% 
  # la table contient les com insulaires hors epci, id_ter de type ZZZZ12345
  mutate(echelle = case_when(echelle == "commune" ~ grep("com", levels(liste_zone$TypeZone), value = T, ignore.case = T),
                             echelle == "département" ~ grep("d.p", levels(liste_zone$TypeZone), value = T, ignore.case = T),
                             echelle == "région" ~ grep("r.g", levels(liste_zone$TypeZone), value = T, ignore.case = T),
                             echelle == "epci" ~ grep("epci", levels(liste_zone$TypeZone), value = T, ignore.case = T),
                             T ~ as.character(echelle)) %>% as.factor())


# commentaires sur dessin fichiers -----------
# Le df conso_elec_179_1 comprend les consommations électriques annuelles à toutes les échelles sauf iris depuis 2008.
# Il correspond à la mise aux référentiels géographiques actuels de l'ensemble de la source *consommations électriques art 179* publiée par le SDES. 
# En raison de la manière de gérer le secret, il ne faut pas utiliser les consommations communales ou à l'iris pour déduire les consommations supra, 
# mais utiliser directement les consommations agrégées à l'échelle EPCI, département ou région. Les conso Fce peuvent être calculées par somme des régions


# ajout total France ---------

source("R/aggreg_non_sommable/aggreg_conso_179.R")

conso_elec_179_frmetro <- filter(conso_elec_179_1, echelle == grep("r.g", levels(liste_zone$TypeZone), value = T, ignore.case = T),
                                 !(id_ter %in% c("01", "02", "03", "04", "05", "06"))) %>%
  group_by(annee) %>%
  aggr_ter_conso() %>%
  mutate(echelle="France", id_ter="FRMETRO", lib_ter="France métropolitaine", sel_reg = FALSE)

conso_elec_179_frentiere <- filter(conso_elec_179_1, echelle == grep("r.g", levels(liste_zone$TypeZone), value = T, ignore.case = T)) %>%
  group_by(annee) %>%
  aggr_ter_conso() %>%
  mutate(echelle="France", id_ter="FRMETRODROM", lib_ter="France métropolitaine et DROM", sel_reg = FALSE)

conso_elec_179_2 <- bind_rows(conso_elec_179_1, conso_elec_179_frmetro, conso_elec_179_frentiere) %>% 
  mutate(date = make_date(annee, 12,31)) %>%
  mutate_if(is.character, as.factor) %>%
  rename(TypeZone = echelle, Zone = lib_ter, CodeZone = id_ter, conso_elec_gwh = conso_gwh)

# rm(conso_elec_179_frentiere, conso_elec_179_frmetro)


# gestion du champ 'sources' à part ----------
# on isole les métadonnées dans un RData à part
metad_conso_elec_179 <- select(conso_elec_179_2, ends_with("Zone"), date, sources, sce_estim)
save(metad_conso_elec_179, file="metadata/metad_conso_elec_179.RData")
# rm(metad_conso_elec_179)


# reprise du calcul ------
levels_communes <- select(communes, CodeZone = DEPCOM, Zone = NOM_DEPCOM) %>% 
  mutate_if(is.factor, as.character) %>% 
  mutate(TypeZone = grep("com", levels(liste_zone$TypeZone), value = T, ignore.case = T)) %>% 
  unite(id_zone, CodeZone, TypeZone, Zone, sep="__") %>%
  pull

conso_elec_179 <- conso_elec_179_2 %>%
  rename_with( ~ gsub("na$","_elec.inconnu", .x)) %>%            
  rename_with( ~ gsub("a$","_elec.agriculture", .x)) %>%
  rename_with( ~ gsub("i$","_elec.industrie", .x)) %>%
  rename_with( ~ gsub("t$","_elec.tertiaire", .x)) %>%            
  rename_with( ~ gsub("r$","_elec.residentiel", .x)) %>%            
  unite(id_zone, CodeZone, TypeZone, Zone, sep="__") %>%
  pivot_longer(names_to = "variable", values_to = "valeur", 'conso_elec.agriculture':conso_elec_gwh) %>%
  mutate(id_zone = fct_expand(id_zone, levels_communes)) %>% 
  complete(id_zone, date, variable, fill = list(valeur = NA)) %>%
  separate(id_zone, c("CodeZone", "TypeZone", "Zone"), sep="__") %>%
  select(date, CodeZone, TypeZone, Zone, variable, valeur) %>%
  pivot_wider(names_from = variable, values_from = valeur) %>% 
  rename_with(tolower)


# versement dans le sgbd/datamart.portrait_territoires -------------

poster_data(data = conso_elec_179,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "cogifiee_conso_elec_179",
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            pk = c("date", "codezone", "typezone"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
            user = "does")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(conso_elec_179), c("date", "codezone", "zone", "typezone"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()

## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "date", "Millesime",
      "codezone", "Code geographique de la zone",
      "typezone", "Type de zone geographique",
      "zone", "Nom de la zone geographique"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "cogifiee_conso_elec_179", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>% 
  # ajout de complement sur la généalogie
  paste0(".\n", "T\u00e9l\u00e9chargement depuis DIDO https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie#lectricit.")
## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "cogifiee_conso_elec_179", 
                user = "does")


rm(list=ls())
