
# chargement_sitadel_densite


# librairies -----------
library(readxl)
library(dplyr)
library(tricky)
library(tidyr)
library(forcats)
library(stringr)
library(lubridate)
library(tidyverse)
library(DBI)
library(RPostgreSQL)
library(datalibaba)
library(googlesheets4)

rm(list = ls())


# réglage des paramètres--------------------------------------------------

# millesimes à importer
millesimes <- c(2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,2018, 2019, 2020)

# redressement des surfaces erronées
red_ip1 <- 10000 # individuel pur, redressé si superficie du terrain/logement supérieur à x m2/logt,avec surf moyenne commune ou dept
red_ip2 <- 150 # individuel pur, redressé si superficie du terrain/logement inférieur à x m2/logt,avec surf moyenne commune ou dept
red_ig1 <- 5000 # individuel groupé, redressé si superficie du terrain/logement supérieur à x m2/logt, avec surf moyenne dept
red_ig2 <- 100 # individuel groupé, redressé si superficie du terrain/logement inférieur à x m2/logt, avec surf moyenne dept
red_col1 <- 1000 # collectif, redressé si superficie du terrain/logement supérieur à x m2/logt, avec surf moyenne dept
red_col2 <- 10 # collectif, redressé si superficie du terrain/logement inférieur à x m2/logt, avec surf moyenne dept


# fonction ----------------

creer_millesimes <- function(millesime) {
  
  # importation des données ---------
  # chargement des données Sitadel_densite --------
  # fichier cree a partir d une requete geokit 3 :
  # "Dossiers publics/geokit3/Regions Pays de la Loire/DREAL/INDICATEURS TER DATA LAB/gk3_sitadel_densite"
  data_mill <- read_excel(paste0("extdata/gk3_sitadel_densite_", millesime, ".xlsx")) %>%
    set_standard_names() %>%
    replace_na(list("nb_lgt_com_individuels_purs" = 0,
                    "nb_lgt_com_individuels_groupes" = 0,
                    "nb_lgt_com_individuels" = 0,
                    "nb_lgt_com_collectifs" = 0,
                    "superficie_du_terrain" = 0,
                    "nb_lgt_com_en_residence" = 0)
               ) %>%
    filter(nb_lgt_com_individuels_purs + nb_lgt_com_individuels_groupes == nb_lgt_com_individuels) %>%
    filter(nb_lgt_com_en_residence == 0) %>%
    filter(nb_lgt_com_individuels + nb_lgt_com_collectifs == nb_lgt_com_total) %>%
    select(-nb_lgt_com_en_residence, -shon_com_creee_totale_des_lgt)
  
  # repérage des données erronées ----------
  data_mill2 <- data_mill %>%
    mutate(surf_a_redr1 = case_when(
      nb_lgt_com_individuels_purs > 0 & (superficie_du_terrain / nb_lgt_com_total) > red_ip1 ~ NA_real_,
      nb_lgt_com_individuels_purs > 0 & (superficie_du_terrain / nb_lgt_com_total) < red_ip2 ~ NA_real_,
      nb_lgt_com_individuels_groupes > 0 & (superficie_du_terrain / nb_lgt_com_total) > red_ig1 ~ NA_real_,
      nb_lgt_com_individuels_groupes > 0 & (superficie_du_terrain / nb_lgt_com_total) < red_ig2 ~ NA_real_,
      nb_lgt_com_collectifs > 0 & (superficie_du_terrain / nb_lgt_com_total) > red_col1 ~ NA_real_,
      nb_lgt_com_collectifs > 0 & (superficie_du_terrain / nb_lgt_com_total) < red_col2 ~ NA_real_,
      TRUE ~ superficie_du_terrain)
      )
  
  # redressement des surfaces erronées --------
  sf_moy <- data_mill2 %>%
    select(-c(numero_permis_concatene, superficie_du_terrain)) %>%
    filter(!is.na(surf_a_redr1))

  sf_moy_ip <- sf_moy %>% # calcul surface moyenne des terrains par commune individuel pur
    filter(nb_lgt_com_individuels_purs > 0) %>%
    group_by(code_commune_insee) %>%
    summarise(surf_moy_terr_ip = sum(surf_a_redr1, na.rm = T) / sum(nb_lgt_com_total, na.rm = T))
  
  sf_moy_dept_ip <- sf_moy %>% # calcul surface moyenne des terrains par dept individuel pur
    filter(nb_lgt_com_individuels_purs > 0) %>%
    group_by(code_departement) %>%
    summarise(surf_moy_terr_ip = sum(surf_a_redr1, na.rm = T) / sum(nb_lgt_com_total, na.rm = T))
  
  sf_moy_dept_ig <- sf_moy %>% # calcul surface moyenne des terrains par dept individuel groupé
    filter(nb_lgt_com_individuels_groupes > 0) %>%
    group_by(code_departement) %>%
    summarise(surf_moy_terr_ig = sum(surf_a_redr1, na.rm = T) / sum(nb_lgt_com_total, na.rm = T))
  
  sf_moy_dept_coll <- sf_moy %>% # calcul surface moyenne des terrains par dept individuel groupé
    filter(nb_lgt_com_collectifs > 0) %>%
    group_by(code_departement) %>%
    summarise(surf_moy_terr_coll = sum(surf_a_redr1, na.rm = T) / sum(nb_lgt_com_total, na.rm = T))
  
  data_mill3 <- data_mill2
  
  data_mill3$surf_a_redr1 <- case_when(
    data_mill3$nb_lgt_com_individuels_purs > 0 & is.na(data_mill3$surf_a_redr1) ~ sf_moy_dept_ip$surf_moy_terr_ip[match(data_mill3$code_departement, sf_moy_dept_ip$code_departement)] * data_mill3$nb_lgt_com_individuels_purs,
    data_mill3$nb_lgt_com_individuels_purs > 0 & is.na(data_mill3$surf_a_redr1) ~ sf_moy_ip$surf_moy_terr_ip[match(data_mill3$code_commune_insee, sf_moy_ip$code_commune_insee)] * data_mill3$nb_lgt_com_individuels_purs,
    data_mill3$nb_lgt_com_individuels_groupes > 0 & is.na(data_mill3$surf_a_redr1) ~ sf_moy_dept_ig$surf_moy_terr_ig[match(data_mill3$code_departement, sf_moy_dept_ig$code_departement)] * data_mill3$nb_lgt_com_individuels_groupes,
    data_mill3$nb_lgt_com_collectifs > 0 & is.na(data_mill3$surf_a_redr1) ~ sf_moy_dept_coll$surf_moy_terr_coll[match(data_mill3$code_departement, sf_moy_dept_coll$code_departement)] * data_mill3$nb_lgt_com_collectifs,
    TRUE ~ data_mill3$surf_a_redr1)
  
  data_mill3 <- data_mill3 %>%
    select(-superficie_du_terrain) %>%
    rename("superficie_du_terrain" = "surf_a_redr1") %>%
    mutate(typelgt = "non") %>%
    filter(!is.na(superficie_du_terrain)) %>% 
    mutate(
      typelgt = case_when(
        nb_lgt_com_collectifs > 0 & libelle_court_type_residence == "non rempli" ~ "col_nr",
        nb_lgt_com_collectifs > 0 & libelle_court_type_residence == "secondaire" ~ "col_rs",
        nb_lgt_com_collectifs > 0 & libelle_court_type_residence == "principale" ~ "col_rp",
        nb_lgt_com_individuels_groupes > 0 & libelle_court_type_residence == "non rempli" ~ "ig_nr",
        nb_lgt_com_individuels_groupes > 0 & libelle_court_type_residence == "secondaire" ~ "ig_rs",
        nb_lgt_com_individuels_groupes > 0 & libelle_court_type_residence == "principale" ~ "ig_rp",
        nb_lgt_com_individuels_purs > 0 & libelle_court_type_residence == "non rempli" ~ "ip_nr",
        nb_lgt_com_individuels_purs > 0 & libelle_court_type_residence == "secondaire" ~ "ip_rs",
        nb_lgt_com_individuels_purs > 0 & libelle_court_type_residence == "principale" ~ "ip_rp"),
      modelgt = case_when(
        nb_lgt_com_collectifs > 0 ~ "col",
        nb_lgt_com_individuels_groupes > 0 ~ "ig",
        nb_lgt_com_individuels_purs > 0 ~ "ip")
      )
  
  # agrégation à la commune des données----------------
  
  # calcul des nr, rp, rs pour coll, ind pur et groupés
  data_mill4a <- data_mill3 %>%
    select(code_commune_insee,
           annee_reelle_de_premiere_ouverture_de_chantier, 
           typelgt, nb_lgt_com_total,
           superficie_du_terrain) %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, typelgt) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T))
  
  # calcul des totaux nr, rp, rs
  data_mill4b <- data_mill3 %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, libelle_court_type_residence) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T)) %>%
    mutate(libelle_court_type_residence = str_replace_all(libelle_court_type_residence, "principale", "total_rp") %>%
             str_replace_all("secondaire", "total_rs") %>%
             str_replace_all("non rempli", "total_nr")) %>%
    rename("typelgt" = "libelle_court_type_residence")
  
  # calcul des nr, rp, rs pour total individuel
  data_mill4c <- data_mill3 %>%
    filter(nb_lgt_com_individuels > 0) %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, libelle_court_type_residence) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T)) %>%
    mutate(libelle_court_type_residence = str_replace_all(libelle_court_type_residence, "principale", "ind_rp") %>%
             str_replace_all("secondaire", "ind_rs") %>%
             str_replace_all("non rempli", "ind_nr")) %>%
    rename("typelgt" = "libelle_court_type_residence")
  
  # calcul des totaux ind pur, ind groupes,et collectif
  data_mill4d <- data_mill3 %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, modelgt) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T)) %>%
    mutate(modelgt = str_replace_all(modelgt, "ip", "total_ip") %>%
             str_replace_all("ig", "total_ig") %>%
             str_replace_all("col", "total_col")) %>%
    rename("typelgt" = "modelgt")
  
  # calcul du total individuel
  data_mill4e <- data_mill4d %>%
    filter(typelgt != "total_col") %>%
    mutate(typelgt2 = "total_ind") %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, typelgt2) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T)) %>%
    rename(typelgt = typelgt2)
  
  # calcul du total général
  data_mill4f <- data_mill4d %>%
    mutate(typelgt2 = "total") %>%
    group_by(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, typelgt2) %>%
    summarise(nb_lgt_com_total = sum(nb_lgt_com_total, na.rm = T),
              superficie_du_terrain = sum(superficie_du_terrain, na.rm = T)) %>%
    rename(typelgt = typelgt2)
  
  # regroupement des tableaux
  data_mill99 <- bind_rows(data_mill4a, data_mill4b, data_mill4c, data_mill4d, data_mill4e, data_mill4f)
  
  data_mill99a <- data_mill99 %>%
    select(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, typelgt, nb_lgt_com_total) %>%
    mutate(typelgt = paste0("id.nb_lgt.", typelgt)) %>%
    rename(valeur = nb_lgt_com_total)
  data_mill99b <- data_mill99 %>%
    select(code_commune_insee, annee_reelle_de_premiere_ouverture_de_chantier, typelgt, superficie_du_terrain) %>%
    mutate(typelgt = paste0("id.superf_terrain.", typelgt)) %>%
    rename(valeur = superficie_du_terrain)
  
  data_mill98 <- bind_rows(data_mill99a, data_mill99b) %>%
    rename(depcom = code_commune_insee, date = annee_reelle_de_premiere_ouverture_de_chantier, variable = typelgt) %>%
    ungroup() %>%
    mutate(date = make_date(date, 12, 31)) %>%
    mutate_if(is.character, as.factor) %>%
    complete(depcom, date, variable, fill = list(valeur = 0))
  
  return(data_mill98)
}
# fin de fonction --------------


# mise en oeuvre de la fonction ---------
sitadel_densite <- map_dfr(millesimes, ~ creer_millesimes(.x)) %>% 
  pivot_wider(names_from = variable,values_from = valeur)


# versement dans le sgbd/datamart.portrait_territoires -------------
poster_data(data = sitadel_densite,
            db = "datamart",
            schema = "portrait_territoires", 
            table = "source_sitadel_densite",
            post_row_name = FALSE, 
            overwrite = TRUE,
            droits_schema = TRUE,
            pk = c("depcom", "date"), # déclaration d'une clé primaire sur la table postée : on ne doit pas avoir deux lignes avec à la fois le même code commune et la meme date
            user = "does")

# METADONNEES------------------------------------

## On récupère la liste des variables qui sont à documenter dans le tableur google sheet à partir du jeu de données posté
var <- setdiff(names(sitadel_densite), c("depcom", "date"))

## récupération du nom du présent script source pour filtrer ensuite le référentiel des indicateurs
nom_script_sce <- rstudioapi::getActiveDocumentContext()$path %>% # utilisation de rstudioapi pour récupérer le nom du présent script 
  basename() %>% # on enlève le chemin d'accès pour ne garder que le nom du fichier
  gsub(pattern = ".R$", "", .) # on enlève l'extension '.R'

## authentification google sheet grâce au .Renviron
gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_deauth()



## chargement du référentiel indicateurs google sheet
metadata_indicateur <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                                  sheet = "indicateurs") %>%
  # on ne garde que les variables concernées par le présent script de chargement
  filter(source == nom_script_sce) %>% 
  # on ajoute l'unité dans le libellé de la variable
  mutate(libelle_variable = paste0(libelle_variable, " (unit\u00e9 : ", unite, ")")) %>% 
  select(variable, libelle_variable) %>% 
  # ajout des libellés pour depcom et date
  bind_rows(
    tribble(
      ~variable, ~libelle_variable,
      "depcom", "Code INSEE de la commune",
      "date", "Millesime"
    )
  )

## Vérification que la documentation des indicateurs est complète
all(var %in% metadata_indicateur$variable) # doit renvoyer TRUE

## Envoi des libellés de variable dans le SGBD
post_dico_attr(dico = metadata_indicateur, table = "source_sitadel_densite", schema = "portrait_territoires",
               db = "datamart", user = "does")

## Récupération des métadonnées de la source
nom_sce <- str_replace(nom_script_sce, "chargement_|ref_|specifique_", "") %>%
  str_replace("indicateur_", "") %>%
  str_replace("_cogiter|_cog$", "")

metadata_source <- read_sheet("https://docs.google.com/spreadsheets/d/1n-dhtrJM3JwFVz5WSEGOQzQ8A0G7VT_VcxDe5gh6zSo/edit#gid=60292277",
                              sheet = "sources") %>%
  filter(source == nom_sce) %>% 
  mutate(com_table = paste0(source_lib, " - ", producteur, ".\n", descriptif_sources)) %>% 
  pull(com_table) %>%
  # ajout de complement sur la généalogie
  paste0(".\n", "Chargement des donn\u00e9es sur Geokit3")

## commentaires de la table

commenter_table(comment = metadata_source,
                db = "datamart",
                schema = "portrait_territoires",
                table = "source_sitadel_densite", 
                user = "does")



